﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RigidbodyExtensions {

    public static void Deactivate(this Rigidbody2D rigidbody) {
        rigidbody.bodyType = RigidbodyType2D.Kinematic;
        rigidbody.simulated = false;
    }

    public static void Activate(this Rigidbody2D rigidbody) {
        rigidbody.bodyType = RigidbodyType2D.Dynamic;
        rigidbody.simulated = true;
    }
}
