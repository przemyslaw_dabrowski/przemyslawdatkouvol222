﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPool<T> where T : Component {
    private readonly T _prefab = default;
    private readonly Transform _poolParent = null;

    private readonly bool _adaptive = false;
    private readonly List<T> _allObjects;
    private List<T> _objectPool;

    public ObjectPool(T prefab, Transform poolParent, int maxLimit, bool adaptive = false) {
        _allObjects = new List<T>(maxLimit);
        _objectPool = new List<T>(maxLimit);
        _prefab = prefab;
        _poolParent = poolParent;
        _adaptive = adaptive;

        for (int i = 0; i < maxLimit; i++) {
            AddNewObject();
        }
    }

    private void AddNewObject() {
        T obj = Object.Instantiate(_prefab, _poolParent);
        _allObjects.Add(obj);
        _objectPool.Add(obj);
    }

    public T Borrow() {
        if (_objectPool.Count > 0) {
            T obj = _objectPool[0];
            _objectPool.RemoveAt(0);
            return obj;
        }
        if (_adaptive) {
            AddNewObject();
            return Borrow();
        }

        Debug.LogError($"Object pool has exceeded its borrow limit!");
        return default;
    }

    public bool Return(T obj) {
        if (_allObjects.Contains(obj)) {
            obj.transform.SetParent(_poolParent);
            _objectPool.Add(obj);
            return true;
        } else {
            Debug.Log($"Object {obj.name} does not belong to the object pool!", obj.gameObject);
            return false;
        }
    }

}
