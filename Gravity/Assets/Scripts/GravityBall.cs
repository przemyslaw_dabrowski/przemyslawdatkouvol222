﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityBall : MonoBehaviour {
    [SerializeField]
    private Rigidbody2D _rigidbody = null;
    [SerializeField]
    private PointEffector2D _pointEffector = null;
    [SerializeField]
    private CircleCollider2D _gravityTrigger = null;
    [SerializeField]
    private CircleCollider2D _collider = null;
    [SerializeField]
    private GameObject _spriteGameObject = null;

    [SerializeField]
    private int _absorptionLimit = 50;
    [SerializeField]
    private float _explosionForceMultiplier = 300f;
    [SerializeField]
    private float _colliderDisableTime = 0.5f;

    private float _originalMass;
    private float _originalGravityRadius;
    private float _originalColliderRadius;

    private List<GravityBall> _absorbedBalls = new List<GravityBall>();

    private float criticalMassTreshold => _originalMass * _absorptionLimit;
    private bool _blockCombining = false;

    public float Mass => _rigidbody.mass;

    private void Awake() {
        _originalMass = _rigidbody.mass;
        _originalGravityRadius = _gravityTrigger.radius;
        _originalColliderRadius = _collider.radius;

        _rigidbody.Deactivate();

    }

    public void Enable() {
        gameObject.SetActive(true);
        _rigidbody.Activate();
    }

    public void Disable() {
        _rigidbody.Deactivate();
        gameObject.SetActive(false);
    }

    public void MoveTo(Vector3 position) {
        position.z = _rigidbody.transform.position.z;
        transform.position = position;
    }

    public void ReverseGravity() {
        _pointEffector.forceMagnitude *= -1f;
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (_blockCombining) {
            return;
        }
        GravityBall other = collision.gameObject.GetComponent<GravityBall>();
        if (other == null) {
            return;
        }
        // that means it's being absorbed
        if (gameObject.activeSelf == false) {
            return;
        }
        Combine(other);
        if (_rigidbody.mass > criticalMassTreshold) {
            Explode();
        }
    }

    private void Combine(GravityBall ball) {
        ball.Disable();
        _absorbedBalls.Add(ball);
        _rigidbody.mass += ball.Mass;
        _gravityTrigger.radius = _originalGravityRadius * _rigidbody.mass;
        _collider.radius = _originalColliderRadius * _rigidbody.mass;
        _spriteGameObject.transform.localScale = new Vector3(_rigidbody.mass, _rigidbody.mass, 1);
    }

    public void Explode() {
        _blockCombining = true;
        float explosionForceModifier = _rigidbody.mass * _explosionForceMultiplier;

        _rigidbody.mass = _originalMass;
        _gravityTrigger.radius = _originalGravityRadius;
        _collider.radius = _originalColliderRadius;
        _spriteGameObject.transform.localScale = new Vector3(1, 1, 1);
        for (int i = _absorbedBalls.Count - 1; i > -1; i--) {
            _absorbedBalls[i].MoveTo(transform.position);
            _absorbedBalls[i].Enable();
            _absorbedBalls[i].Explode();
            _absorbedBalls.RemoveAt(i);
        }
        Vector2 randomDirection = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)) * explosionForceModifier;
        StartCoroutine(DisableColliders());
        _rigidbody.AddForce(randomDirection, ForceMode2D.Impulse);
    }

    private IEnumerator DisableColliders() {
        _gravityTrigger.enabled = false;
        yield return new WaitForSeconds(_colliderDisableTime);
        _gravityTrigger.enabled = true;
        _blockCombining = false;
    }
}
