﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GravityBallSpawner : MonoBehaviour {

    private ObjectPool<GravityBall> _gravityBallPool = null;
    [SerializeField]
    private Transform _gravityBallPoolParent = null;
    [SerializeField]
    private Transform _gravityBallActiveParent = null;
    [SerializeField]
    private GravityBall _gravityBallPrefab = null;
    [SerializeField]
    private float _spawnDelayInSeconds = 0.25f;
    [SerializeField]
    private int _maxGravityBalls = 250;

    // used the original unity text to avoid importing TMPRo
    [SerializeField]
    private Text _textCounter = null;

    private List<GravityBall> _allBalls;

    [SerializeField]
    private Camera _camera = null;

    private WaitForSeconds _wait;

    private void Awake() {
        _wait = new WaitForSeconds(_spawnDelayInSeconds);
        _allBalls = new List<GravityBall>(_maxGravityBalls);
        _gravityBallPool = new ObjectPool<GravityBall>(_gravityBallPrefab, _gravityBallPoolParent, _maxGravityBalls, true);
        StartCoroutine(SpawnBalls());
    }

    private IEnumerator SpawnBalls() {
        while (_allBalls.Count < _maxGravityBalls) {
            GravityBall ball = _gravityBallPool.Borrow();
            ball.transform.SetParent(_gravityBallActiveParent);
            ball.MoveTo(GetRandomPosition());
            ball.Enable();
            _allBalls.Add(ball);
            _textCounter.text = $"Balls count: {_allBalls.Count}";

            yield return _wait;
        }

        for (int i = 0; i < _allBalls.Count; i++) {
            _allBalls[i].ReverseGravity();
        }

    }

    public Vector3 GetRandomPosition() {
        Rect rect = _camera.pixelRect;

        Vector3 position = new Vector3(Random.Range(rect.xMin, rect.xMax), Random.Range(rect.yMin, rect.yMax), 0f);
        return _camera.ScreenToWorldPoint(position);
    }

}
