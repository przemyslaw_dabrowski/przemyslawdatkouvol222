﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    [SerializeField]
    private Camera _camera;

    private Vector3 _originalPosition;

    private Transform _currentFollower = null;

    public static CameraController Instance { get; private set; }

    private void Awake() {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            _originalPosition = transform.position;
        } else {
            Destroy(gameObject);
            Debug.LogError($"There can be one instance of a singleton of type CameraController!");
        }
    }

    public Vector3 GetCameraMousePosition() {
        return _camera.ScreenToWorldPoint(Input.mousePosition);
    }

    public void FollowTarget(Transform target) {
        _currentFollower = target;
    }

    public void UnfollowTarget() {
        _currentFollower = null;
        transform.position = _originalPosition;
    }

    private void LateUpdate() {
        if (_currentFollower == null) {
            return;
        }

        Vector3 newPosition = _currentFollower.position;
        newPosition.z = _originalPosition.z;
        transform.position = newPosition;
    }
}
