﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slingshot : MonoBehaviour {
    [SerializeField]
    private Transform _bulletSpawnPoint = null;

    [SerializeField]
    private Bullet _bulletPrefab = null;

    [SerializeField]
    private LineDrawer _lineDrawer = null;

    [SerializeField]
    private float _distanceForceMultiplier = 3f;

    private Bullet _currentBullet = null;
    private bool _isDragging = false;
    private bool _isMouseOver = false;

    private void OnMouseEnter() {
        _isMouseOver = true;

        if (_currentBullet == null) {
            _currentBullet = Instantiate(_bulletPrefab);
            _currentBullet.MoveTo(_bulletSpawnPoint.position);
            _lineDrawer.SetTarget(_currentBullet.transform);
        }
    }

    private void OnMouseExit() {
        _isMouseOver = false;

        if (_isDragging) {
            return;
        }
        if (_currentBullet != null) {
            Destroy(_currentBullet.gameObject);
            _currentBullet = null;
        }
    }

    private void OnMouseDown() {
        _isDragging = true;
        _lineDrawer.Enable();

    }

    private void OnMouseUp() {
        _isDragging = false;
        _lineDrawer.Disable();
        Fire();
    }

    private void Fire() {
        Vector3 mousePos = CameraController.Instance.GetCameraMousePosition();
        Vector3 bulletPosition = _currentBullet.transform.position;
        float distance = Vector3.Distance(mousePos, bulletPosition);
        float forceModifier = distance * _distanceForceMultiplier;
        var result = (bulletPosition - mousePos).normalized * forceModifier;
        CameraController.Instance.FollowTarget(_currentBullet.transform);
        _currentBullet.Fire(result, CameraController.Instance.UnfollowTarget);
        _currentBullet = null;
    }

}
