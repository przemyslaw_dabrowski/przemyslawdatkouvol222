﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawer : MonoBehaviour {

    [SerializeField]
    private LineRenderer _lineRenderer = null;

    private Vector3[] _lineVectors = new Vector3[2];
    private Transform _currentTarget = null;

    public void Enable() {
        _lineRenderer.enabled = true;
    }

    public void Disable() {
        _lineRenderer.enabled = false;
    }

    public void SetTarget(Transform transform) {
        _currentTarget = transform;

    }

    private void LateUpdate() {
        if (_currentTarget == null) {
            return;
        }
        if (_lineRenderer.enabled == false) {
            return;
        }
        SetLinePositions();
    }

    private void SetLinePositions() {
        _lineVectors[0] = _currentTarget.position;
        _lineVectors[1] = CameraController.Instance.GetCameraMousePosition();
        _lineRenderer.SetPositions(_lineVectors);
    }
}
