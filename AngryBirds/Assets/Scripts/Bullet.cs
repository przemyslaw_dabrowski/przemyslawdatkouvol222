﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    [SerializeField]
    private Rigidbody2D _rigidbody = null;
    [SerializeField]
    private Collider2D _collider = null;

    private System.Action _onHitCallback = null;

    private void Awake() {
        _rigidbody.bodyType = RigidbodyType2D.Kinematic;
        _collider.enabled = false;
    }

    public void MoveTo(Vector2 position) {
        _rigidbody.MovePosition(position);
    }

    public void Fire(Vector2 forceToAdd, System.Action onHitCallback = null) {
        _onHitCallback = onHitCallback;
        _collider.enabled = true;
        _rigidbody.bodyType = RigidbodyType2D.Dynamic;
        _rigidbody.AddForce(forceToAdd, ForceMode2D.Impulse);
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        _onHitCallback?.Invoke();
        _onHitCallback = null;
    }
}
